# T-Deck Template
**** This is a living document it will be updated as we progress in learning these devices ****

This Project is for the LILLYGO T-Deck with or without the LORA module fitted.
The one we have has the 915mhz module fitted.
https://www.lilygo.cc/products/t-deck?variant=43087936389301


The major use case for the T-Deck is with the Meshtastic firmware/ecosystem.
https://meshtastic.org/

At its core the T-Deck is an ESP32-S3 microcontroller with a ton of add-ons
which makes it ripe for the DIY electronics community.

The Creator LILLYGO has a fantastic github repo with all the resources to use all the functions of 
the T-Deck  https://github.com/Xinyuan-LilyGO/T-Deck

But There is little to no simplifed how-tos or commented code. So I desided to create this repo
and files. with my own findings and projects. Below is a list of all the add-ons and the information I 
know so far. Also see the Pictures directory for images of the device working with custom code. 


LILLYGO T-Deck with 915mhz LORA module. 

ESP32-S3 Microcontroller with wifi and bluetooth built in.  - We are useing the Arduino IDE

320x240 SPI TFT touch screen -  We are currently useing the TFT_eSPI library *touch not tested yet*

Blackberry Keyboard with Trackball (ACE button)  --  Not tested yet. there are librarys for them

Speaker - Unsure at this time if theres a driver. or how to drive it.

Micophone - Unsure how to use this at this time. 

Battery Managment chip -- unsure how to read battery state/status

LORA module  -  We have did a few experiments with this.

Add-on by Me

Ubloc Neo 6m GPS unit. - Working with the Tiny GPS library






/* LILLYGO T-Deck Custom code. 2/24/24 */
// Working as of 2/24/24 1pm EST


// Image files can be converted to arrays using the tool here:
// https://notisrac.github.io/FileToCArray/
// To use this tool:
//   1. Drag and drop file on "Browse..." button
//   2. Tick box "Treat as binary"
//   3. Click "Convert"
//   4. Click "Save as file" and move the header file to sketch folder
//   5. Open the sketch in IDE
//   6. Include the header file containing the array (panda.h in this example)

#define PWR_UP 10  // Power Pin for T-Deck addons

#include <WiFi.h>
#include <WiFiClient.h>
#include <PNGdec.h>   // Libaray to decode PNG files
#include "PDG21.h" // Image is stored here in an 8 bit array Change filename for other images


const char* host = "TDeck";           // WIFI DETAILS
const char* ssid = "ShadowTronics";
const char* password = "chiman123sabre";

#include <TFT_eSPI.h> // Screen library    // See the DisplaySetup.txt for details on how to setup the TFT library
#include <SPI.h>      //  for screen


#define MAX_IMAGE_WIDTH 240 // Adjust for your images

int16_t xpos = 0;         //Cursor Position  NEED to study on setting CENTER, LEFT , RIGHT postions
int16_t ypos = 0;

PNG png;                         // PNG decoder inatance // Invoke custom Library
TFT_eSPI tft = TFT_eSPI();       // Invoke custom library

void pngDraw(PNGDRAW *pDraw) {              // This is needed to Draw the image to the screen
  uint16_t lineBuffer[MAX_IMAGE_WIDTH];
  png.getLineAsRGB565(pDraw, lineBuffer, PNG_RGB565_BIG_ENDIAN, 0xffffffff);
  tft.pushImage(xpos, ypos + pDraw->y, pDraw->iWidth, 1, lineBuffer);
}

void showPic() {   // My Custom Function to draw the png image to the screen                                              
int16_t rc = png.openFLASH((uint8_t *)PDG21, sizeof(PDG21), pngDraw);
  if (rc == PNG_SUCCESS) {
    Serial.println("Successfully opened png file");
    Serial.printf("image specs: (%d x %d), %d bpp, pixel type: %d\n", png.getWidth(), png.getHeight(), png.getBpp(), png.getPixelType());
    tft.startWrite();
    uint32_t dt = millis();
    rc = png.decode(NULL, 0);
    Serial.print(millis() - dt); Serial.println("ms");
    tft.endWrite();
 }
}


void setup() {
  pinMode(PWR_UP, OUTPUT);   // setup power pin state
  digitalWrite(PWR_UP, HIGH); // set pin High to power on addons
  delay(500);   // 1/2 second delay for everything to wake up.
  Serial.begin(115200); // Start Serial communication for debug
// We start by connecting to a WiFi network
  Serial.println();
  Serial.println("******************************************************");
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
   delay(500);
   Serial.print(".");
   }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  tft.init();              // We Init the Display
  tft.setRotation(1);      // On INIT, Screen is in Landscape. Rotation 1 sets to portrait.
  tft.fillScreen(TFT_BLACK);
  showPic();  // We call my custom function to display the image!
  //tft.setTextSize(3);                     // Work in Progress
  //tft.setTextColor(TFT_BLUE, TFT_BLACK);
  //tft.println("WiFi connected");
  //tft.println("IP address: ");
  //tft.println(WiFi.localIP());
  
}


void loop() {
  // put your main code here, to run repeatedly:

}
